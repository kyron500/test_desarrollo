/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Details;

import com.tpsv.SecDev.Entities.UsuariosEntities;
import com.tpsv.SecDev.Repositories.UsuariosRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author kaliadmin
 */
@Service
public class UsuariosDetailsService implements UserDetailsService{

    @Autowired
    UsuariosRepository usuariosRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    
        Optional<UsuariosEntities> usuario = usuariosRepository.findByUsername(username);
        
        usuario.orElseThrow(() -> new UsernameNotFoundException("Usuario no Encontrado"));
        
        return usuario.map(UsuariosDetails::new).get();
        
    }
    
}
