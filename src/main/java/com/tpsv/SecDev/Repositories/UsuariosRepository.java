/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Repositories;

import com.tpsv.SecDev.Entities.UsuariosEntities;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author kaliadmin
 */
public interface UsuariosRepository extends JpaRepository<UsuariosEntities, Integer> {

    Optional<UsuariosEntities> findByUsername(String username);
    
}
