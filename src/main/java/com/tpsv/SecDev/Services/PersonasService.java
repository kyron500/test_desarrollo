/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Services;

import com.tpsv.SecDev.Entities.PersonasEntities;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author kaliadmin
 */

public interface PersonasService {
    
    List<PersonasEntities> getAllPersonas();
    void savePersonas(PersonasEntities personas);
    PersonasEntities getPersonasById(int id);
    void deletePersonaById(int id);
    Page<PersonasEntities> generarPagina(int nroPag, int filasPag, String sortStr, String sortDir);
    
    
}
