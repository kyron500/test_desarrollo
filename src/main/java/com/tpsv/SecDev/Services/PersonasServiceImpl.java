/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Services;

import com.tpsv.SecDev.Entities.PersonasEntities;
import com.tpsv.SecDev.Repositories.PersonasRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author kaliadmin
 */
@Service
public class PersonasServiceImpl implements PersonasService {
    
    @Autowired
    private PersonasRepository personasRepository;
    
    @Override
    public List<PersonasEntities> getAllPersonas() {
        return personasRepository.findAll();
    }

    @Override
    public void savePersonas(PersonasEntities personas) {
        this.personasRepository.save(personas);
    }

    @Override
    public PersonasEntities getPersonasById(int id) {
        Optional<PersonasEntities> optional = personasRepository.findById(id);
        PersonasEntities personas = null;
        if (optional.isPresent()){
            personas = optional.get();
        } else {
            throw new RuntimeException("Persona no encontrada!");
        }
        
        return personas;
        
    }

    @Override
    public void deletePersonaById(int id) {
        this.personasRepository.deleteById(id);
    }

    @Override
    public Page<PersonasEntities> generarPagina(int nroPag, int filasPag, String sortStr, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortStr).ascending() :
                Sort.by(sortStr).descending();
        
        Pageable pageable = PageRequest.of(nroPag-1, filasPag, sort);
        return this.personasRepository.findAll(pageable);
        
     }
    
    
    
    
    
}
