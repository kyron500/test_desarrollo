/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Controllers;

import com.google.common.hash.Hashing;
import com.tpsv.SecDev.Details.UsuariosDetails;
import com.tpsv.SecDev.Entities.PersonasEntities;
import com.tpsv.SecDev.Services.PersonasService;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PersonasController {
    
    @Autowired
    private PersonasService personasService;
    
    //mostrar list de personas en la raiz
    @GetMapping("/")
    public String viewRaiz(Model model, HttpServletResponse response) {
        
        String secret = "wCht35h0GS5Y2ZKSwEcx8nkR10kyne43WkXeB2eBMsVACUm0ZYtgF6EVdjkFLNWdudpaJSbaG0L7lR6x";
        
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        String nombreUsuario;
        
        if (principal instanceof UsuariosDetails) {
            nombreUsuario = ((UsuariosDetails)principal).getUsername();
        } else {
            nombreUsuario = principal.toString();
        }
        
        String md5hash = Hashing.md5()
                .hashString(nombreUsuario,StandardCharsets.UTF_8)
                .toString();
        
        String sha256hash = Hashing.sha256()
                .hashString(md5hash+secret, StandardCharsets.UTF_8)
                .toString();
        
        //Cookie cookie = new Cookie("userHash",sha256hash);
        
        //response.addCookie(cookie);
        
        return generarPagina(1,"nombre","asc",model);
    }
    
    @GetMapping("/formularioNuevaPersona")
    public String formularioNuevaPersona(Model model) {
        PersonasEntities personas = new PersonasEntities();
        model.addAttribute("personas", personas);
        
        return "nueva_persona";
    }

    @PostMapping("/savePersona")
    public String savePersona(@ModelAttribute("personas") PersonasEntities personas ) {
        
        personasService.savePersonas(personas);
        return "redirect:/";
        
    }
    
    @GetMapping("/formularioActualizacion/{id}")
    public String formularioActualizacion(@PathVariable(value = "id") int id, Model model) {
        PersonasEntities personas = personasService.getPersonasById(id);
        
        model.addAttribute("personas", personas);
        return "actualizar_persona";
    }
    
    @GetMapping("/eliminarPersona/{id}")
    public String eliminarPersona(@PathVariable (value="id") int id) {
        this.personasService.deletePersonaById(id);
        return "redirect:/";
        
    }
    
    @GetMapping("/pagina/{nroPagina}")
    private String generarPagina(@PathVariable (value="nroPagina") int nroPagina,
            @RequestParam("sortStr") String sortStr,
            @RequestParam("sortDir") String sortDir,
            Model model) {
        int filasPag = 5;
        
        Page<PersonasEntities> page = personasService.generarPagina(nroPagina, filasPag, sortStr, sortDir);
        List<PersonasEntities> listPersonas = page.getContent();
        
        model.addAttribute("paginaActual", nroPagina);
        model.addAttribute("totalPaginas", page.getTotalPages());
        model.addAttribute("totalElementos", page.getTotalElements());
        
        model.addAttribute("sortStr",sortStr);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("sortDirReves", sortDir.equals("asc") ? "desc" : "asc");
        
        model.addAttribute("listPersonas", listPersonas);
        return "index";
        
        
    }
    
    
}
