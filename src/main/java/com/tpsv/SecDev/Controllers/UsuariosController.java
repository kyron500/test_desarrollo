/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tpsv.SecDev.Controllers;

import com.tpsv.SecDev.Entities.UsuariosEntities;
import com.tpsv.SecDev.Repositories.UsuariosRepository;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UsuariosController {
        
    private final UsuariosRepository usuariosRepository;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    public UsuariosController(UsuariosRepository usuariosRepository) {
        this.usuariosRepository = usuariosRepository;
    }
    
    
    @Transactional
    @PostMapping("/users")
    @ResponseBody
    public List<UsuariosEntities> users(@RequestParam String api_key) {
        
        //TODO-COMPROBAR_LOGICA_API_KEY
        
        
        
        Query q1 = entityManager.createNativeQuery("SELECT COUNT (*) FROM usuarios WHERE api_key = (?)")
                .setParameter(1, api_key);
        
        if ( ((Number)q1.getSingleResult()).intValue() != 0) {
            
            return usuariosRepository.findAll();
        }
        
        return Collections.emptyList();
        
    }
    
    
    /*
    @Transactional
    @PostMapping(value = "/users",headers="Content-Type=application/x-www-form-urlencoded")
    @ResponseBody
    public String insertarUsuario(@RequestParam String nombre, @RequestParam String apellido) {
        
        try {
            entityManager.createNativeQuery("INSERT INTO usuarios(nombre,apellido) VALUES (?,?)")
                    .setParameter(1, nombre)
                    .setParameter(2, apellido)
                    .executeUpdate();

            return "OK!";
            
        } catch (Exception e) {
            
            return "Not OK!";
            
        }
    
        
    }
    */
    
    
}
